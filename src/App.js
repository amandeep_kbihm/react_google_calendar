/* global gapi */
import React from 'react';
import moment from 'moment';
// import Calendar from './Calendar';
import {Form, Button} from 'react-bootstrap';
import {
  CLIENT_ID as clientId,
  DISCOVERY_DOCS as discoveryDocs,
  SCOPES as scope,
} from './constans';

var leftDiv = {
  'border-style': 'ridge',
  'padding':'2%'
};

class App extends React.Component {
  state = {
    events: [],
    isSignedIn: false,
    title: "",
    start_date: "",
    end_date: "",
    description: "",
    editing: false,
    editId: "",

    edit:{
      id: "",
      title: "",
      start_date: "",
      end_date: "",
      description: ""
    }
  };

  componentDidMount() {
    // Init gapi
    gapi
    .load('client:auth2', () => {
      gapi
      .client
      .init({clientId, discoveryDocs, scope})
      .then(() => {
      // Listen for sign-in state changes.
        gapi.auth2.getAuthInstance().isSignedIn.listen(this.updateSigninStatus);
        this.updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
      });
    });
  }

  insertEvent = async () => {
    const {
      id,
      title,
      start_date,
      end_date,
      description} = this.state;
    const event = {
      'summary': title,
      'location': '800 Howard St., San Francisco, CA 94103',
      'description': description,
      'start': {
        'dateTime': new Date(start_date).toISOString(),
        'timeZone': 'America/Los_Angeles'
      },
      'end': {
        'dateTime': new Date(end_date).toISOString(),
        'timeZone': 'America/Los_Angeles'
      }
    }
    const {result} = await  gapi.client.calendar.events.insert({
      'calendarId': 'primary',
      'resource': event
    });
    const sd = new Date(start_date).toISOString()
    const s_date = new Date(sd)
    const ed = new Date(end_date).toISOString()
    const e_date = new Date(ed)
    const record = {allDay: false, id: result.id, title: title, start: s_date, end: e_date, description: description}
    this.setState({
      events: [
        record,
        ...this.state.events
      ],
      title: "",
      start_date: "",
      end_date: "",
      description: ""
      })
  };

  updateEvent = async () => {
    const {
      id,
      title,
      start_date,
      end_date,
      description} = this.state.edit;
    const event = {
      'summary': title,
      'location': '800 Howard St., San Francisco, CA 94103',
      'description': description,
      'start': {
        'dateTime': new Date(start_date).toISOString(),
        'timeZone': 'America/Los_Angeles'
      },
      'end': {
        'dateTime': new Date(end_date).toISOString(),
        'timeZone': 'America/Los_Angeles'
      }
    }
    
    var request = gapi.client.calendar.events.patch({
      'calendarId': 'primary',
      'eventId': id,
      'resource': event
    });

    request.execute(function (event) {
      console.log(event);
    });
  }

  deleteEvent = async (eventId) => {
    let my = this;
      var request = gapi.client.calendar.events.delete({
        calendarId: 'primary',
          'eventId': eventId
      });
      request.execute(function(response) {
          if(response.error || response == false){
              alert('Error');
          }
          else{
              // alert('Success'); 
              const remainingItems = my.state.events.filter (item => {  
                return item.id !== eventId;
              });
              my.setState({
                events: remainingItems
              });
          }
      });
  }

 insertEvents = async () => {
  const event = {
    'summary': 'Google I/O 2015',
    'location': '800 Howard St., San Francisco, CA 94103',
    'description': 'A chance to hear more about Google\'s developer products.',
    'start': {
      'dateTime': '2019-06-28T09:00:00-07:00',
      'timeZone': 'America/Los_Angeles'
    },
    'end': {
      'dateTime': '2019-06-28T17:00:00-07:00',
      'timeZone': 'America/Los_Angeles'
    },
    'recurrence': [
      'RRULE:FREQ=DAILY;COUNT=2'
    ],
    'attendees': [
      {'email': 'lpage@example.com'},
      {'email': 'sbrin@example.com'}
    ],
    'reminders': {
      'useDefault': false,
      'overrides': [
        {'method': 'email', 'minutes': 24 * 60},
        {'method': 'popup', 'minutes': 10}
      ]
    }
  };
  
  const {result} = await  gapi.client.calendar.events.insert({
    'calendarId': 'primary',
    'resource': event
  });
 }

  transformEventsToBigCalendar = (items) => {
    return items.map(
      ({id, summary: title, start: originalStart, end: originalEnd, description: description}) => {
        let allDay;
        let start;
        let end;

        if (originalStart.date === undefined) {
          allDay = false;
          start = moment(originalStart.dateTime).toDate();
          end = moment(originalEnd.dateTime).toDate();
        } else {
          allDay = true;
          start = moment(originalStart.date).toDate();
          end = moment.utc(originalEnd.date).toDate();
        }

        return {allDay, id, title, start, end, description};
      }
    );
  }

  getEvents = async () => {
    try {
      const {result: {items}} = await gapi.client.calendar.events.list({
        calendarId: 'primary',
        timeMin: (new Date()).toISOString(),
        showDeleted: false,
        singleEvents: true,
        maxResults: 20,
        orderBy: 'startTime'
      });
      console.log("events:", items)
      return this.transformEventsToBigCalendar(items)
    } catch (e) {
      console.error(e);
    }
  }

  handleLogin = () => {
    gapi.auth2.getAuthInstance().signIn();
  }

  handleLogout = () => {
    gapi.auth2.getAuthInstance().signOut();
  }

  updateSigninStatus = async (isSignedIn) => {
    if (isSignedIn) {
      const events = await this.getEvents();
      this.setState({
        events,
        isSignedIn: true
      });
    } else {
      this.setState({
        events: [],
        isSignedIn: false
      });
    }
  }

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleEditChange = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      ...this.state,
      edit:{
       ...this.state.edit,
        [name]: value
      }
    });
  }

  formatDate(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes+ ampm;
    return date.getMonth()+1 + "-" + date.getDate() + "-" + date.getFullYear() + ", " + strTime;
  }

  handleSubmit = (event) => {
    event.preventDefault();
    this.insertEvent();
  }

  handleUpdate = (event) => {
    event.preventDefault();
    this.updateEvent()
    this.setState({editing: false, eventId: "", edit:{
      id: "",
      title: "",
      start_date: "",
      end_date: "",
      description: ""
    }})
    console.log("state :", this.state)
  }

  editFunction = (event) => {
    this.setState({ editing: true, editId:  event.id, edit:{
      id: event.id,
      title: event.title,
      start_date: event.start_date,
      end_date: event.end_date,
      description: event.description
    }});
  }

  CancelEditFunction = (event) => {
    this.setState({ editing: false, editId:  "", edit:{
      id: "",
      title: "",
      start_date: "",
      end_date: "",
      description: ""
    }});
  }

  deleteFunction = (eventId) => {
    this.deleteEvent(eventId)
  }

  render() {
    const {
      events,
      isSignedIn,
    } = this.state;
    return (
      isSignedIn
      ? <React.Fragment>
          <Button label="Logout" onClick={this.handleLogout} >
              Logout
          </Button>
          <div className="container">
            <div className="row">
              {events &&
                <div className="col-sm-6">
                  {events.map((event) => (
                    <div className="row"  style={leftDiv}>
                      <div className="col-sm-8">
                      {this.state.editing}
                        { (this.state.editing == true & this.state.editId == event.id) ? 
                        <Form onSubmit={this.handleUpdate}>
                          <a href="#" onClick={() => this.CancelEditFunction(event)} >
                              Cancel
                          </a>
                          <Form.Group controlId="formBasicTitle">
                            <Form.Label>Title</Form.Label>
                            <Form.Control name="title" value={this.state.edit.title} onChange={this.handleEditChange} type="text" placeholder="Enter Title" />
                          </Form.Group>

                          <Form.Group controlId="formBasicStartDate">
                            <Form.Label>Start Date</Form.Label>
                            <Form.Control name="start_date" value={this.state.edit.start_date} onChange={this.handleEditChange} type="date" placeholder="Enter Start Date" />
                          </Form.Group>

                          <Form.Group controlId="formBasicEndDate">
                            <Form.Label>End Date</Form.Label>
                            <Form.Control name="end_date" value={this.state.edit.end_date} onChange={this.handleEditChange} type="date" placeholder="Enter End Date" />
                          </Form.Group>

                          <Form.Group controlId="formBasicDescription">
                            <Form.Label>Description</Form.Label>
                            <Form.Control name="description" value={this.state.edit.description} onChange={this.handleEditChange} type="text" placeholder="Description" />
                          </Form.Group>

                          <Button variant="primary" type="submit">
                            Update
                          </Button>
                        </Form>
                        :
                        <li key={event.id}>
                          <ul> id: {event.id} </ul>
                          <ul>title: {event.title} </ul>
                          <ul> start_date: {this.formatDate(new Date(event.start))}</ul>
                          <ul> end_date: {this.formatDate(new Date(event.end))}</ul>
                          <ul> description: {event.description}</ul>
                        </li>
                        }
                        
                      </div>
                      <div className="col-sm-4">
                        <a href="#" onClick={() => this.editFunction(event)} >
                          edit
                        </a>
                        <br/>
                        <a href="#" onClick={() => this.deleteFunction(event.id)} >
                          Delete
                        </a>
                      </div>
                    </div>
                  ))}
                </div>
              }
              {/* <Calendar {...{events}}/> */}
              <div className="col-sm-6">
                <Form onSubmit={this.handleSubmit} style={leftDiv}>
                  <Form.Group controlId="formBasicTitle">
                    <Form.Label>Title</Form.Label>
                    <Form.Control name="title" value={this.state.title} onChange={this.handleInputChange} type="text" placeholder="Enter Title" />
                  </Form.Group>

                  <Form.Group controlId="formBasicStartDate">
                    <Form.Label>Start Date</Form.Label>
                    <Form.Control name="start_date" value={this.state.start_date} onChange={this.handleInputChange} type="date" placeholder="Enter Start Date" />
                  </Form.Group>

                  <Form.Group controlId="formBasicEndDate">
                    <Form.Label>End Date</Form.Label>
                    <Form.Control name="end_date" value={this.state.end_date} onChange={this.handleInputChange} type="date" placeholder="Enter End Date" />
                  </Form.Group>

                  <Form.Group controlId="formBasicDescription">
                    <Form.Label>Description</Form.Label>
                    <Form.Control name="description" value={this.state.description} onChange={this.handleInputChange} type="text" placeholder="Description" />
                  </Form.Group>
                  <Button variant="primary" type="submit">
                    Submit
                  </Button>
                </Form>
              </div>
            </div>
          </div>
        </React.Fragment>
      : 
      <Button variant="primary" label="Login" onClick={this.handleLogin}>
        Login
      </Button>
    );
  }
}
export default App;

